---
order: 1
---

# Introduction: OpenStack nad Windows Server

[PDF filer](36-GitDaGitt-Erik_Hjelmas.pdf)

![png fil](ntnu_hoeyde_eng copy.png)

The goal of this course is for you to primarily learn two topics:
*Windows infrastructure: theory and practice* and *How to get the basics
right*. Based on my gut feeling I claim that 90% of servers you use on
the Internet are Linux-servers while 90% of important servers on the
internal networks of companies are Windows-servers. These percentages
are of course not accurate, but my point is that most organizations who
have their own IT-infrastructure base it on a Windows infrastructure
with Active Directory at its core. Modern IT-infrastructures are
commonly implemented in private or public clouds, so we need to start
out by learning a bit about cloud computing as well. We will soon move
on the "Getting the basics right" which in our context (which can be
considered a technical subcontext of NSM’s "Grunnprinsipper for
IKT-sikkerhet 2.0" ) means studying how to do the following in a
cloud-based Windows infrastructure:

  - Backup and restore

  - Identity management and access control

  - Configuration management

  - Software package management

  - Logging and monitoring

  - Fast (automatic) redeploy/installation/provisioning

## Cloud Computing

IaaS, PaaS, SaaS in
figure [1.1](#fig:intro:e0ca0ef4d460441c9ea8e17366eb01c8).

![IaaS, PaaS,
SaaS.<span label="fig:intro:e0ca0ef4d460441c9ea8e17366eb01c8"></span>](/home/erikhje/office/kurs/secsrv/01-intro/tex/../img/Cloud_computing.pdf)  
<span>[CC-BY-SA-3.0](https://creativecommons.org/licenses/by-sa/3.0) By
Sam Johnston</span>

Cloud computing is about offering computing services on the Internet
without us knowing exactly which server is offering the service (it’s
just somewhere in "the cloud"). Cloud computing services were offered
firstly by Amazon Web Services (IaaS) in 2006, by Google App Engine in
2008 (PaaS) and Microsoft Azure in 2010, and in general we categorize
the services as

  - IaaS  
    Infrastructure as a Service: compute (virtual machines), network and
    storage

  - PaaS  
    Platform as a Service: run-time environments (e.g. Kubernetes),
    databases, web servers, object storage (files access over http, not
    "virtual hard disk" like in IaaS)

  - SaaS  
    Software as a Service: e.g. email (Gmail), monitoring (Grafana
    cloud), LMS (Blackboard Learn SaaS)

Sometimes you will see the term *serverless computing* which means that
a cloud computing customer does not manage or directly use any virtual
machines, the customer only uses services that mostly are categorized as
PaaS. When a cloud computing customer makes full use of modern cloud
computing and uses all accompanying modern software development and
management practices and processes (Agile, DevOps, DevSecOps, etc), the
customer is said to be *cloud native* . The "opposite" of being cloud
native is when you just copy your on-premise data center into the cloud
(just replicating your network and servers with the same setup of
services without taking true advantage of what the cloud offers).

### Characteristics

Cloud Computing Characteristics in
figure [\[fig:intro:936c3ca8173a4e32b17a362c46f96258\]](#fig:intro:936c3ca8173a4e32b17a362c46f96258).

  - Dynamic  
    you can create and destroy/delete infrastructures as needed

  - Self-service  
    you manage everything yourself in software (instead of buying
    hardware)

  - Pay-as-you-go  
    you only pay for the resources you need and use

Cloud computing is based on virtualization and shared resources (shared
physical servers, networks and storage). The key characteristics of
cloud computing that have lead to this paradigm shift in information
technology is dynamic, self-service and pay-as-you-go.

### Security and Privacy

There is not much special about security and privacy when moving to the
cloud. Mostly it is the same concerns as when you are running your
IT-systems on-premise, but many times you have to pay particular
attention to where you end up storing data due to legal reasons (are you
allowed to store your data in the country where the cloud computing
servers are located?).

Security and Privacy in
figure [\[fig:intro:7f701446d34b48829b9c37e6ecc9c843\]](#fig:intro:7f701446d34b48829b9c37e6ecc9c843).

  - Privacy: storing data on someone else’s computers

  - All issues related to outsourcing (possibly loosing control)

  - Inventory overview (asset management) can be challenging in a very
    dynamic environment

  - Fast DevOps-style replace of services can be beneficial for patching

  - Otherwise mostly same issues as on-premise
    (patch/update,backups,access control,logs,monitoring)

Something you have probably already learned, but is always worth
repeating, is how to use cryptography in practice for protecting a
simple file. This technique can also be used for a large file tree if
you package/zip the file tree into one file before encrypting it. Nice
cross-platform tools with open implementations of open widely used
crypto algorithms are [7-zip](https://www.7-zip.org) and
[OpenSSL](https://www.openssl.org). Example using the crypto-algorithm
AES-256 (example from a Linux command line session):


{% highlight shell %}
    # 7-zip:
    $ dpkg -S $(which 7z)      # which package installs 7-zip?
    p7zip-full: /usr/bin/7z
    $ 7z a -p confidential.txt.7z confidential.txt # encrypt
    $ 7z e confidential.txt.7z                     # decrypt
    
    # OpenSSL:
    $ dpkg -S $(which openssl) # which package installs OpenSSL?
    openssl: /usr/bin/openssl
    $ openssl enc -aes-256-cbc -a -pbkdf2 \
       -in confidential.txt -out confidential.txt.enc    # encrypt
    $ openssl enc -aes-256-cbc -a -pbkdf2 \
       -d -in confidential.txt.enc -out confidential.txt # decrypt
{% endhighlight %}

## OpenStack

The original announcement of OpenStack included the mission statement:

> To produce the ubiquitous Open Source Cloud Computing platform that
> will meet the needs of public and private clouds regardless of size,
> by being simple to implement and massively scalable.

The open source project OpenStack is a collection of software components
which can be composed into a public or private cloud. An organization
can choose to implement only the components that it needs. NTNU has a
few different OpenStack implementations. We will use the largest
implementation which is called
[SkyHiGh](https://www.ntnu.no/wiki/display/skyhigh)\[1\]

OpenStack Components in
figure [1.2](#fig:intro:c7b7103620b545759cf42173d49d1c95).

![OpenStack
Components.<span label="fig:intro:c7b7103620b545759cf42173d49d1c95"></span>](/home/erikhje/office/kurs/secsrv/01-intro/tex/../img/OpenStack_map.pdf)  
<span>[CC-BY-SA-4.0](https://creativecommons.org/licenses/by-sa/4.0) via
Wikimedia Commons</span>

Demo: Horizon, Heat, Cinder, Swift

SkyHiGh implements the components (might be expanded by the time you
read this) Horizon, Magnum, Heat, Nova, Swift, Cinder, Neutron, Keystone
and Glance. Each of these components can run on their own set of
physical servers. We say "set of physical servers" because in a secure
core infrastructure we need all components to be redundant. *Redundancy*
means that you have \(N\) installations of the same component, where
\(N>1\). Each component offers one or more *services* which are
reachable on a specific *port number* and IP address. E.g. when you want
to create a virtual disk (to attach to your virtual machine) in SkyHiGh,
and you click on "Volumes" and "Create Volume", what is actually
happening is that the web application (Horizon) sends a command ("create
a virtual hard disk of size ...") to port number 8776  on one of the
physical servers who have Heat installed. Which commands we can send to
a service is decided by the API of that service. *The API (Application
Programming Interface)* defines what you can ask a service about, it
basically defines which "commands" you can use towards a service.

### SkyHiGh

SkyHiGh is two racks full of physical servers with redundant
installations of many of the available OpenStack open source components.

A SkyHiGh Rack in
figure [1.3](#fig:intro:06fbabee0c5644d8a5bda650ca645749).

![A SkyHiGh
Rack.<span label="fig:intro:06fbabee0c5644d8a5bda650ca645749"></span>](/home/erikhje/office/kurs/secsrv/01-intro/tex/../img/skyhigh.jpg)

When we use SkyHiGh in this course, we are interacting with the
different components in the following way:

  - Horizon  
    is the application that we use when we log in on
    <https://skyhigh.iik.ntnu.no>.

  - Heat  
    is the *orchestration* service that we use when we click on
    "Orchestration". We use Heat for creating a *stack* which is
    OpenStack terminology for an infrastructure composed of different
    cloud components (virtual machine instances, networks, security
    groups, routers, etc). It is called a stack because we can treat it
    (create/delete) as one unit (one integrated stack) even though it
    has several separate cloud components.

  - Cinder  
    (an example of IaaS) is the *block storage* service that we use when
    we want to create a virtual disk.

  - Swift  
    (an example of PaaS) is the *Object storage* service that we use
    when we want to store public or private files.

These are the only components we will be using directly since we will
not create virtual machines or networks manually, only through
orchestration (Heat). We will of course interact with the other
components as well, but only to retrieve information, e.g. when we click
on "Compute", Horizon will ask the Nova service for information about
the running virtual machine instances we have created through Heat.

SkyHiGh is only available to employees and students at NTNU: it is a
*private cloud*. OpenStack is also used for *public clouds*  similar to
Amazon Web Services (AWS), Microsoft Azure or Google Cloud, but the
largest OpenStack installation is probably still the private cloud at
CERN with more than 300K CPU cores  .

## Windows Server

> *Note: in the rest of this chapter, we illustrate the theory with
> examples from PowerShell. We will learn PowerShell in the next chapter
> so don’t worry if you don’t understand the exact syntax used in the
> examples. The author is fully aware that some PowerShell-constructions
> you will see in this chapter might look a bit Greek to you. You should
> return to this chapter and study these examples later when you learn
> more about PowerShell.*

Computers can often be divided into the categories *client* and
*server*. This terms also apply to computer programs: e.g. a web browser
is a client program that communicates with a web server. For operating
systems running on physical hardware ("bare metal") or running in a
virtual machine, a client is characterized by having a user present who
is using interactive programs (like you do when you use your laptop,
tablet or phone). A server is characterized by offering services to
clients or other servers. Windows 10 is a version of the Windows
operating system configured to be a used as a client, while Windows
Server is a version of the Windows operating system configured to be,
you guessed it, a server. A client also has services running, but these
are mostly for internal use.

### Process and Service

Process, Thread, Service in
figure [1.4](#fig:intro:c2fc13e740dc42d1ba05df11f8926b8c).

![Process, Thread,
Service.<span label="fig:intro:c2fc13e740dc42d1ba05df11f8926b8c"></span>](/home/erikhje/office/kurs/secsrv/01-intro/tex/../img/procthreadsrv.pdf)

When you execute a program on an operating system, the running program
is called a *process*. E.g. when you run Notepad you execute the code in
the file notepad.exe, and you get a running process called notepad (btw
the following code is PowerShell which you will learn more about soon):

{% highlight powershell %}
    PS> notepad.exe
    PS> Get-Process notepad -IncludeUserName
    
    Handles      WS(K)   CPU(s)     Id UserName               ProcessName
    -------      -----   ------     -- --------               -----------
        216      20352     0.14   6288 DC1\Admin              notepad
{% endhighlight %}

Notepad is a typical *interactive process* meant to be used directly by
a user. We can also see that a process has an *owner*, in this example
with notepad it is owned by me since I am the user `Admin` logged in on
the *host* `dc1` (a computer with an operating system is many times
called a *host* since it is the host of many processes, files, users,
etc.).

A process can be single-threaded or multithreaded, meaning it can have
one or more *threads*. All the programs you have written so far in your
studies (and probably all the ones you will write this semester) are
single-threaded. In other words, when you don’t actively create any
threads in your code you end up with what is called a single-threaded
program (when executed it is called a single-threaded process). A thread
is a lightweight process living inside a process. You will learn much
more about this in the Operating Systems course, but for now it is good
to know that the concept exists, and that a process which would like to
use more than one CPU-core at the same time has to be multithreaded. The
operating system allocates the CPU-cores to threads, not processes. You
can see how many threads each process has with the following PowerShell
command line:

{% highlight powershell %}
    Get-Process | 
      Select-Object -Property Name, ID, `
        @{Name='ThreadCount';Expression ={$_.Threads.Count}} | 
      Sort-Object -Property ThreadCount -Descending
{% endhighlight %}

Opposed to an interactive process, a *service* or a *service process* is
a process running in the "background" and typically started as part of
the boot-up process of the computer. A service runs independently of any
logged-in users. Services are mostly owned by separate system accounts.
We can roughly divide services into *host-internal services* and
*network services*:

  - host-internal service  
    is a service that is supporting the operating system or doing some
    kind of local system maintenance, e.g Windows Update, Task
    Scheduler. Some of the really important host-internal services we
    call *system processes* (these are core to the functioning of the
    Windows operating system).

  - network service  
    is a service that is listening on a port for incoming connections
    from a client, e.g. Remote Desktop Services who is listening by
    default on port 3389.

Seeing which service processes are listening for connection from the
network is many times an interesting exercise both for troubleshooting
and for security/forensics:

{% highlight powershell %}
    Get-NetTCPConnection |
      Select-Object -Property LocalAddress,LocalPort,State,OwningProcess,`
      @{Name='ProcessName';Expression={(Get-Process `
        -Id $_.OwningProcess).ProcessName}},`
      @{Name='ServiceName';Expression={(Get-CimInstance -ClassName `
        Win32_Service -Filter "ProcessID=$($_.OwningProcess)").Name}} | 
      Format-Table -AutoSize
{% endhighlight %}

Service processes have to comply with the protocol/rules of the *Service
Control Manager* (a special *system process* called `services`). This
means that all service processes have to be able to be maintained by
accepting messages from the Service Control Manager such as Stop, Start,
Suspend, Resume, etc. We can use PowerShell to ask the service control
manager to send these messages:

{% highlight powershell %}
    PS> Get-Command *service | Format-Table -Property Name
    
    Name
    ----
    Get-Service
    New-Service
    Remove-Service
    Restart-Service
    Resume-Service
    Set-Service
    Start-Service
    Stop-Service
    Suspend-Service
{% endhighlight %}

Service processes can be stand-alone processes, or they can be "wrapped"
by themselves or together with other services in a *service host
process* with process name `svchost`. The easiest way of seeing all the
services and which services are hidden in a service host process is by
using the old cmd-program `tasklist /svc`. When a service is wrapped in
a service host process it means that the service is loaded into the
service host process from a DLL (Dynamic Link Library), so the service
does not exist a separate process, it only exists inside the service
host process. The reason for having these service host processes was to
save resources since each process takes up a significant amount of
memory on Windows. With modern 64-bit Windows running on 64-bit hardware
memory is not such a big issue anymore, and most service host processes
only contain a single service nowadays .

### Accounts

Accounts in
figure [\[fig:intro:7a3607e3e1cd4ae1bce9293c75846ea9\]](#fig:intro:7a3607e3e1cd4ae1bce9293c75846ea9).

  - User accounts

  - System accounts

As mentioned previously, a process is owned by an account on the system.
To get a quick overview in PowerShell (must be run as Administrator):

{% highlight powershell %}
    Get-Process -IncludeUserName |
      Select-Object -Property Name,UserName,ID |
      Sort-Object -Property UserName,ID
{% endhighlight %}

Files and directories are also owned by accounts on the system. We need
to have ownership of all objects on the system if we are going have any
kind of access control/security. To see which local accounts that are
present on your Windows (we can have "network accounts" as well, but we
will learn about that later):

{% highlight powershell %}
    # List user accounts
    Get-LocalUser
    # or
    Get-CimInstance Win32_UserAccount
    # List system accounts
    Get-CimInstance Win32_SystemAccount
{% endhighlight %}

We can also use PowerShell to see a list of differences between User and
System accounts. The difference is basically that user accounts have
several password-fields since they are meant for interactive login
sessions while system accounts are used for services. Notice how the
"SideIndicator" points towards the right for the "DifferenceObject"
which is the user account in the variable `$u`. This means that this
property (the "InputObject") only exists in `$u` and not in `$s`:

{% highlight powershell %}
    PS> $s = Get-CimInstance Win32_SystemAccount | Get-Member
    PS> $u = Get-CimInstance Win32_UserAccount | Get-Member
    PS> Compare-Object -ReferenceObject $s -DifferenceObject $u
      
      InputObject                                 SideIndicator
      -----------                                 -------------
      uint AccountType {get;}                     =>
      bool Disabled {get;set;}                    =>
      string FullName {get;set;}                  =>
      bool Lockout {get;set;}                     =>
      bool PasswordChangeable {get;set;}          =>
      bool PasswordExpires {get;set;}             =>
      bool PasswordRequired {get;set;}            =>
      PSStatus {Status, Caption, PasswordExpires} =>
      PSStatus {Status, SIDType, Name, Domain}    <=
{% endhighlight %}

### Configuration Data

#### Registry

The Registry  is a database that stores most of the configuration on a
Windows host. The purpose is to avoid configuration stored in files many
places in the file system.

The Registry in
figure [\[fig:intro:1c43de52989e496a95d00b770d4d8dd9\]](#fig:intro:1c43de52989e496a95d00b770d4d8dd9).

  - HKEY\_LOCAL\_MACHINE (HKLM)  
    Config for local computer, SAM, SECURITY,  
    SOFTWARE and SYSTEM, files in `%SystemRoot%\System32\config`

  - HKEY\_CURRENT\_CONFIG (HKCC)  
    link to  
    `HKLM\System\CurrentControlSet\`  
    `Hardware Profiles\Current`

  - HKEY\_USERS (HKU)  
    each user profile actively loaded on the machine

  - HKEY\_CURRENT\_USER (HKCU)  
    link to currently logged on user in `HKU`

  - HKEY\_CLASSES\_ROOT (HKCR)  
    a compilation of `HKCU\Software\Classes` and  
    `HKLM\Software\Classes`

<!-- end list -->

  - HKLM contains the sub keys SAM (user account database), SECURITY,
    SOFTWARE and SYSTEM which have corresponding files on disk. It also
    contains the sub key HARDWARE which is generated at runtime (similar
    to the `proc` filesystem on Linux).

  - HKCC just links into HKLM.

  - HKU is user specific config data for each active user profile (note:
    a user profile can be active without the user being logged in).

  - HKCU just links into HKU.

  - HKCR stores file associations and is just linked into HKLM and HKCU.

Edit the registry using the GUI regedit.exe or with command-line
PowerShell:  
`Get-ItemProperty` and `Set-ItemProperty`.

*On a bigger scale/in production, you almost never change the registry
directly, instead you change it indirectly using config management tools
such as group policy.* Some examples of retrieving data from the
registry using PowerShell:

{% highlight powershell %}
    # List installed software that can be uninstalled:
    Get-ChildItem `
      HKLM:\software\microsoft\windows\currentversion\uninstall |
      ForEach-Object {Get-ItemProperty $_.PSPath} | 
      Format-Table -Property DisplayName
    # List all local firewall rules (messy)
    # (combine into one line):
    Get-ItemProperty HKLM:\System\CurrentControlSet\Services\
      SharedAccess\Parameters\FirewallPolicy\FirewallRules
{% endhighlight %}

#### WMI

Windows Management Instrumentation in
figure [\[fig:intro:ae546be2c1c34db8b780337818952f03\]](#fig:intro:ae546be2c1c34db8b780337818952f03).

  - CIM (Common Information Model)

  - WMI: objects with properties and methods grouped into namespaces

  - Mostly *config data from registry* and *performance* data (from the
    operating system)

The CIM standard defines a large set of management objects/tables which
is supposed to represent “everything that can be managed on a host” in a
standardized way. These CIM objects have properties and methods. WMI is
Microsoft’s implementation of CIM with WMI providers (COM objects
implemented as DLLs, approx 100 in total) which provides access to these
CIM-based objects. Since the number of CIM objects/tables is extremely
large, they are grouped into *namespaces*.

Note: much of the information that can be retrieved and manipulated
through WMI comes from the registry (or “can also be found in the
registry”). But you can also access information like performance
counters (from the operating systems data structures) through WMI.

{% highlight powershell %}
    # Show the list of all namespaces:
    Get-CimInstance -Namespace root -ClassName __Namespace
    
    # List all classes in namespace:
    Get-CimClass -Namespace root/CIMV2
    
    # List all instances (objects) of class Win32_Processor:
    Get-CimInstance Win32_Processor
    
    # List all properties of a specific instance (object):
    Get-CimInstance Win32_Processor -Filter "DeviceID='CPU0'" | 
      Select-Object -Property *
    
    # Show a specific property of a specific instance (object):
    (Get-CimInstance Win32_Processor `
      -Filter "DeviceID='CPU0'").LoadPercentage
{% endhighlight %}

Make sure you become comfortable with the terminology *namespace*,
*class* and *object*. You will encounter these terms many times in
different courses this semester:

  - Namespace  
    When you have a high number of classes/objects/variables, and you
    need to group them together you introduce a namespace, in other
    words just a high-level category to separate groups of
    classes/objects/variables from each other. An analogy can be postal
    codes (postnummer): e.g. 2843 is the postal code for Eina but only
    in the *namespace Norway*, in other countries the postal code 2843
    represents something else if it is defined.

  - Class  
    A class is a *definition* of an object. It is abstract. When you
    learned programming in C you had different data types (int, float,
    char, etc). A data type is similar to a class, it is just a
    definition of something, e.g. an int is a number that uses four
    Bytes of space in memory.

  - Object  
    An object is when you have to declare a concrete instance of a
    class. It is something that actually exists, and you use, similar to
    when you declare a variable in C, e.g. `int i;` (int is similar to a
    class, i is similar to an object).

Notice also that we use the word *instance* in many situations when we
talk about the concrete realization of something. In the text above we
talk about the objects as "instances of a class" and when we use clouds
like SkyHiGh we call the virtual machines "instances of an image" (or
mostly we just say "instance", the "image" refers to the disk image of a
base operating system that we create our virtual machine from, e.g.
Ubuntu 20.04 or Windows Server 2022).

When studying computer science you have to deal with the fact that some
terms have different meanings dependent on the context they are used in.
Many times we use the different terms for the same thing: e.g. in this
course (and later in the study program) you will see that when referring
to a virtual machine in SkyHiGh, we will say *instance*, *VM*, *host*,
*server* or specify the name we have given the virtual machine, e.g.
*DC1*.

### Using Windows Server

When you log into a Windows Server you are automatically greeted with
the application *Server Manager*. We will not use Server Manager much
(we will later use the new Windows Admin Center instead), but we will
use it to get a quick overview of server status and to quickly find the
tools we are looking (see the menu *Tools*).

Server Manager in
figure [1.5](#fig:intro:fd740bef17e64344aca5a182287cd3e4).

![Server
Manager.<span label="fig:intro:fd740bef17e64344aca5a182287cd3e4"></span>](/home/erikhje/office/kurs/secsrv/01-intro/tex/../img/servermgr.png)  

Notice that one of the first things Server Manager proposes that we do
is to add *Roles* and *Features*:

  - Role  
    A server role is a set of software programs which make up a specific
    function for multiple users or other computers within a network.

  - Feature  
    Features are software programs that, although they are not directly
    parts of roles, can support or augment the functionality of one or
    more roles, or improve the functionality of the server, regardless
    of which roles are installed.

  - Capability  
    Note quite sure, but seems like a special kind of software
    installation, e.g OpenSSH.client is an installed capability.

Right Click Start-button in
figure [1.6](#fig:intro:f847b58da6a04f7aba88a08d44c9af18).

![Right Click
Start-button.<span label="fig:intro:f847b58da6a04f7aba88a08d44c9af18"></span>](/home/erikhje/office/kurs/secsrv/01-intro/tex/../img/rightclickstart.png)  

It is also worth noting that many of the same GUI-tools for management
of a Windows Server can be accesses by right-clicking on the
Start-button.

#### Sysinternals

Sysinternals in
figure [\[fig:intro:cab484a3a76f4a6caa8920c666e9bfdf\]](#fig:intro:cab484a3a76f4a6caa8920c666e9bfdf).

  - File and Disk (`AccessChk`)

  - Networking (`AD Explorer`, `TCPview`)

  - Process (`Process Explorer`)

  - Security (`SysMon`, `SDelete`, `AccessChk`)

  - System info (`coreinfo`, `RAMMap`)

  - Misc (`BgInfo`, `ZoomIt`)

Sysinternals  is a collection of very useful tools created and updated
over the last 25 years mostly by Mark Russinovich, let’s install the
entire collection and use some of them in the exercises:

{% highlight powershell %}
    # if first time installing with chocolatey:
    Set-ExecutionPolicy Bypass -Scope Process -Force
    [System.Net.ServicePointManager]::SecurityProtocol =
      [System.Net.ServicePointManager]::SecurityProtocol -bor 3072
        iex ((New-Object System.Net.WebClient).
          DownloadString('https://chocolatey.org/install.ps1'))
    # always check the status of a package before installing, if ok:
    choco install -y sysinternals
{% endhighlight %}

If the package web page says e.g. "Some Checks Have Failed or Are Not
Yet Complete", make sure you know what it means. Read the text under
"Details" and also take a look at [Chocolatey Community Package
Repository](https://docs.chocolatey.org/en-us/information/security#chocolatey-community-package-repository).

If you really want to dig deep into how Windows works (and use the
sysinternals utilities to do it), Mark Russinovich has co-authored the
book "Windows Internals"  for you.

### Windows Command Line Interface (CLI)

Before 2006 Windows only had the good old command line interface (CLI)
`cmd` (which is still present on all versions of Windows). In 2006
PowerShell arrived, and has since been continuously growing in
popularity and is now the standard CLI on Windows hosts. Most of what we
need to do on a Windows host, we can do with PowerShell, but sometimes
PowerShell do not offer the same functionality as the traditional
[Windows
Commands](https://docs.microsoft.com/en-us/windows-server/administration/windows-commands/windows-commands)
from the `cmd` CLI, fortunately these work just as well in PowerShell as
in `cmd`.

CLI is Power\! in
figure [1.7](#fig:intro:d1bc85437f124e8db3bab47b413bde8a).

![CLI is
Power\!.<span label="fig:intro:d1bc85437f124e8db3bab47b413bde8a"></span>](/home/erikhje/office/kurs/secsrv/01-intro/tex/../img/sudo.png)

GUI vs CLI in
figure [\[fig:intro:a1cdf0aa0a5c4c7ca08aab986bcc044d\]](#fig:intro:a1cdf0aa0a5c4c7ca08aab986bcc044d).

*“I need to do something `N` times on `M` hosts”*

  - `N=1, M=1`  
    Use a GUI

  - `N>1, M=1`  
    Use CLI

  - `N=1, M>1`  
    Use CLI

  - `N>1, M>1`  
    Use CLI

<!-- end list -->

  - In many situations you do not want a GUI

  - With CLI you can script your tasks with *consistency\!*

*Next topic, let’s go PowerShell\!*

Any Relevant Jobs? in
figure [\[fig:intro:93d40b342e994993937c07e73472b021\]](#fig:intro:93d40b342e994993937c07e73472b021).

[Vil du hacke Norge i landets mest spennende miljø for
IT-sikkerhet?](https://arbeidsplassen.nav.no/stillinger/stilling/3ed907e9-8fbd-404d-ae59-08ff0e6e5e9e)

## Review questions and problems

1.  In which time period did AWS EC2, Google App Engine, Microsoft Azure
    and OpenStack appear?
    
    1.  1996-2000.
    
    2.  2001-2005.
    
    3.  2006-2010.
    
    4.  2011-2015.

2.  What do you consider as a major *privacy* concern/threat when you
    use a public cloud? Give an example including a technical measure (a
    technical solution) to prevent it.

3.  What is *orchestration*? Which component in OpenStack provides this
    service?

4.  What is a *service* in Windows? Describe briefly.

5.  What is a *WMI* (Windows Management Instrumentation? Describe
    briefly.

6.  Let’s explore the topic of roles and features:
    
    1.  Use Server Manager to install the *role* "Web Server (IIS)".
    
    2.  Which *feature* is required during installation of this role?

7.  Let’s explore the topic of processes and services:
    
    1.  Install Sysinternals. Start Process Explorer \[2\].
    
    2.  Sort processes by CPU-usage, which process consumes the most CPU
        and why do you think that is?
    
    3.  How much memory (RAM, Working Set) does PowerShell (`pwsh`) use?
        How much of that memory is private only for PowerShell?
    
    4.  Apparently, processes are arranged in a hierarchy
        (tree-structure) where some processes are parents of others.
        Click three times on the Process-tab to switch between views.
        Which process is the parent of PowerShell? Describe what that
        process is.
    
    5.  Right-click on the Explorer-process, choose "Properties", how
        many threads does this process have?
    
    6.  Which command-line have started the Explorer-process?
    
    7.  Start the Sysinternals tool TCPview. Which process is listening
        on port 80?
    
    8.  Go back to Process Explorer, right click and "Properties" on the
        process you found who is listening on port 80. Choose the
        TCP/IP-tab, check and uncheck the box "Resolve addresses" to see
        two different views. We do this so you can confirm the
        information you saw with TCPview. Do you get a good gut-feeling
        about what you have found out about the process who is listening
        on port 80?
    
    9.  Add the "Command line"-column to Process Explorer (View, Select
        Columns, Process Image), expand the width of the column so you
        see the entire text, can you find a command line containing
        `iissvcs`? Which services does this svchost-process provide?

8.  (**KEY PROBLEM**) Explain how the Registry, WMI, services and the
    Sysinternals tools relate to each other.

## Lab tutorials

1.  Do the [Basic Infrastructure
    Orchestration](https://gitlab.com/erikhje/dcsg1005/-/blob/master/heat-labs.md)
    exercise with the template  
    [single\_windows\_server.yaml](https://gitlab.com/erikhje/heat-mono/-/blob/master/single_windows_server.yaml).
    You have completed the exercise when you have logged in to the host,
    done this weeks practical parts of "Review questions and problems",
    and then deleted the stack. Note: the way you are supposed to work
    in this course is:
    
    1.  to create an infrastructure (a stack) like you do here
    
    2.  document/backup anything you do of value in your infrastructure
    
    3.  delete the infrastructure (the stack)
    
    Sometimes you will do these three items for just one week like now,
    and sometimes you will let your infrastructure exist for a couple of
    weeks. *Always be prepared for quickly deleting and recreating your
    stack, that’s how we take advantage of what the cloud offers us, and
    helps us get used to modern DevOps thinking: frequent changes.*

# PowerShell

## PowerShell

PowerShell in
figure [\[fig:powershell:12bc4ac62c89495596efd5a822238e82\]](#fig:powershell:12bc4ac62c89495596efd5a822238e82).

  - See [separate document on
    PowerShell](https://gitlab.com/erikhje/dcsg1005/blob/master/powershell.md),
    for this week and the following two weeks we focus on [PowerShell 1:
    Standalone
    Host](https://gitlab.com/erikhje/dcsg1005/-/blob/master/powershell.md#powershell-1-standalone-host)

Note: this PowerShell-document is meant to cover everything we need to
know about PowerShell throughout the semester. It contains more material
than what is possible to learn in just one week. Read everything in
[PowerShell 1: Standalone
Host](https://gitlab.com/erikhje/dcsg1005/-/blob/master/powershell.md#powershell-1-standalone-host)
this week to get the initial overview, and focus on the lab tutorial and
review questions and problems. For now, you just need to practice on a
single host. When we get to chapter five (week five), you will need to
start studying the second part of the PowerShell-document called
[PowerShell 2: Domain-joined hosts and
Remoting](https://gitlab.com/erikhje/dcsg1005/-/blob/master/powershell.md#powershell-2-domain-joined-hosts-and-remoting).

## Review questions and problems

1.  Find out which PowerShell cmdlet are mapped to the aliases `cd`,
    `echo`, `cat`, `cp`, `rm` and `sort`. Try all these six cmdlets.

2.  Write a command line which recursively outputs all directories in
    your home directory. It should not list files, only directories  
    (hint: `Get-Help -Online Get-ChildItem`).

3.  Create a variable `$golf` with the value `Viktor Hovland`. Use
    `Write-Output` to print "My golf hero Viktor Hovland" using this
    variable. Advanced: Now try to use the `Split()` method of the
    variable to print only "My golf hero Hovland" (hint: you can use the
    `Split()`-method just like this without any arguments).

4.  Use `Get-Member` to list all properties and methods in the objects
    you get from `Get-ChildItem`. Pipe to `more` to page-by-page (by
    hitting space) or line-by-line (by hitting enter). Repeat with
    `Get-Process` instead of `Get-ChildItem`.

5.  Write a PowerShell command line that will list all files in the
    directory `C:\Windows` that are larger than 10KB. Then add to the
    pipeline:
    
    1.  Make the output sorted based on file size (Length)
    
    2.  Make the output show only the three largest files
    
    3.  Make the output show only file name, file size, last access time
        and last write time

6.  Using
    [splatting](https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.core/about/about_splatting),
    write a command line which recursively outputs all files (not
    directories, only files) in  
    `C:\Windows\System32\LogFiles` and also have the parameter
    ErrorAction set to SilentlyContinue.

7.  (**KEY PROBLEM**) Write a command line which prints the name of all
    directories (from your current directory) which contain at least 10
    files/subdirectories.

8.  (**KEY PROBLEM**) Write a command line which outputs all processes
    which have the property  
    `StartTime` within the last hour.

## Lab tutorials

1.  Study and work through all exercises included in [PowerShell 1:
    Standalone
    Host](https://gitlab.com/erikhje/dcsg1005/-/blob/master/powershell.md#powershell-1-standalone-host).

# Storage, Backup, Restore

## Storage and Windows

File-level vs Block-level in
figure [3.1](#fig:storage:6098c780db01414882f7f92364ecb42c).

![File-level vs
Block-level.<span label="fig:storage:6098c780db01414882f7f92364ecb42c"></span>](/home/erikhje/office/kurs/secsrv/03-storage/tex/../img/filevsblock.pdf)

We are used to dealing with the concept of a *file*.

  - A File  
    has two components: metadata and data. What the metadata component
    consists of depends on the file system, but most file system
    supports owner, permissions, timestamps and size (size is sometimes
    called length since the file contents can be treated as an array of
    Bytes and this array has a length). In addition, the metadata
    contains information about how to reach the data (the actual file
    contents). In the simplest form this can be to just have all the
    block addresses of the data blocks stored in the metadata.

Many of us have used a storage device like a USB-harddrive, memory-stick
or memory-card. A storage device consists of blocks. Before we can use
it for files, it needs to have a file system written to it. When we work
with data storage, backup and restore, we sometimes work with files at
the *file-level* and sometimes we work at the *block-level*. It is
important to understand this difference, especially in the cloud and
other virtualized infrastructures. When you use a virtual machine, the
block-level storage device is commonly a file. This means that you are
using a *storage stack* where the file you save in the virtual machine
is stored in a file system which is on a block device, and the block
device is actually just a file on a physical server which is stored on a
file system on a block device (and the block device might be the actual
hard drive on the server or a block device accessed over the network
from a storage solution like Ceph  which is the one used in SkyHiGh).

The following is a summary of key concepts (including corresponding
PowerShell cmdlets and command lines) we need to know about:

Some Terminology in
figure [\[fig:storage:8f5aa22385d44df8bf70a3e372ac6574\]](#fig:storage:8f5aa22385d44df8bf70a3e372ac6574).

|                    |                           |
| :----------------- | :------------------------ |
| Disk               | `Get-Disk`                |
| Partition table    | `Initialize-Disk`         |
| Partition          | `Get-Partition`           |
| Volume             | `Get-Volume`              |
| File system        | `ntfsinfo` (Sysinternals) |
| File               | `Get-ChildItem`           |
| File: ACL with ACE | `Get-Acl`                 |

  - Demo: Disk management GUI-tool

<!-- end list -->

  - Disk  
    (`Get-Disk`) What Windows perceives as a physical disk (SSD or HDD).
    In our case this is what we create in OpenStack (in the OpenStack
    component called “Cinder”) as a “Volume” and attach to a server. In
    other words: inside our server this is seen as a physical disk and
    we treat it as a physical disk, but outside our server (in our
    cloud) this is a volume.

  - Partition table  
    (`Initialize-Disk`) On a new physical disk we create a partition
    table. This is written in the first part of the disk, and is either
    the old-style “Master Boot Record (MBR)” or the newer (and what we
    typically use today) “GUID Partition Table (GPT)”.

  - Partition  
    (`Get-Partition`) A partition is a part of a disk (it is defined in
    the partition table).

  - Volume  
    (`Get-Volume`) A volume is unfortunately not a term that is clearly
    defined. Sometimes it is the same as a partition, sometimes it is
    multiple partitions joined together, sometimes we talk about a
    physical and a logical volume, etc..., it depends on the context.
    For now, just think of it as either a partition or a group of
    partitions treated as one unit. *A volume is something we can create
    a file system on.* (We can also create a file system directly on a
    partition, but we typically have a volume layer between the file
    system and the partition).

  - File system  
    (`Get-Volume | Format-Table -Property DriveLetter,FileSystemType`) A
    file system is the data structure we write to a volume (or a
    partition in some cases) that allows us to store files and
    directories/folders. On Windows the most common file system is NTFS.

  - File  
    (`Get-ChildItem`) A file consists of metadata (owner, permissions,
    timestamps, etc.) and data (the actual contents of the file). A
    directory is just a special kind of file.

  - File Security: ACL with ACE  
    (`(Get-Acl file).Access`) All files and folders have and Access
    Control List (ACL). An ACL is a list of Access Control Entries
    (ACE). Each ACE defined a user or group and what permission (read,
    write, execute, append, etc.) they have. Each entry can be of type
    allow or deny (deny can be used to exclude a permission that a user
    otherwise would have). ACLs are scanned by the operating system
    (Windows) in order and the first ACE that match the access attempted
    is used.

## Backup: Why?

Chapter 2.9 "Etabler evne til gjenoppretting av data" of NSM’s
"Grunnprinsipper for IKT-sikkerhet 2.0"  is worth reciting in full. We
will address most of these measures and submeasures in the rest of this
chapter.

Measure 2.9.1:

> Legg en plan for regelmessig sikkerhetskopiering av alle
> virksomhetsdata. En slik plan bør som minimum beskrive
> 
> 1)  Hvilke data som skal sikkerhetskopieres.
> 
> 2)  Regelmessighet på sikkerhetskopiering av ulike data, basert på
>     verdi.
> 
> 3)  Ansvar for sikkerhetskopiering av ulike data.
> 
> 4)  Prosedyrer ved feilet sikkerhetskopiering.
> 
> 5)  Oppbevaringsperiode for sikkerhetskopier.
> 
> 6)  Logiske og fysiske krav til sikring av sikkerhetskopier.
> 
> 7)  Krav til gjenopprettingstid for virksomhetens ulike systemer og
>     data (se prinsipp 4.1 - Forbered virksomheten på håndtering av
>     hendelser).
> 
> 8)  Godkjenningsansvarlig(e) for planen.

Measure 2.9.2:

> Inkluder sikkerhetskopier av programvare for å sikre gjenoppretting.
> Dette inkluderer (som minimum)
> 
> 1)  sikkerhetskonfigurasjon ref. prinsipp 2.3 - Ivareta en sikker
>     konfigurasjon og
> 
> 2)  maler for virtuelle maskiner og
> 
> 3)  "master-images" av operativsystemer og c)
>     installasjonsprogramvare.

Measure 2.9.3:

> Test sikkerhetskopier regelmessig ved å utføre gjenopprettingstest for
> å verifisere at sikkerhetskopien fungerer.

Measure 2.9.4:

> Beskytt sikkerhetskopier mot tilsiktet og utilsiktet sletting,
> manipulering og avlesning.
> 
> 1)  Sikkerhetskopier bør være separert fra virksomhetens
>     produksjonsmiljø. Se bl.a. prinsipp 2.1 - Ivareta sikkerhet i
>     anskaffelses- og utviklingsprosesser.
> 
> 2)  Tilgangsrettigheter til sikkerhetskopier bør begrenses til kun
>     ansatte og systemprosesser som skal gjenopprette data.
> 
> 3)  Det bør jevnlig tas offline sikkerhetskopier som ikke kan nås via
>     virksomhetens nettverk. Dette for å hindre tilsiktet/utilsiktet
>     sletting eller manipulering.
> 
> 4)  Sikkerhetskopier bør beskyttes med kryptering når de lagres eller
>     flyttes over nettverket. Dette inkluderer ekstern
>     sikkerhetskopiering og skytjenester.

Backup System = Data Restoration System in
figure [\[fig:storage:bd6a1790adf3474685f2c60143137bbc\]](#fig:storage:bd6a1790adf3474685f2c60143137bbc).

  - Why?
    
      - Archival purposes
    
      - Disk failure
    
      - Theft
    
      - Accidental deletion
    
      - Ex-employees (insider threats)
    
      - Malware: *ransomware*
    
      - Natural disasters, bomb, el.magn.pulse., etc

A Backup system should really be called a "Data restoration system"
since backups are useless if we cannot do restore of backed up data. The
backup system plays an integral part in the overall Disaster Recovery
Plan (in the NSM-document  this is in section four and called "planverk
for hendelseshåndtering som ivaretar behovet for virksomhetskontinuitet
ved beredskap og krise."). It is important to think about why we should
do backups because it directly influences which strategy we should
choose. If the most common reason for restore requests is accidential
deletion, then we should put some effort into making sure users have
easy access to basic file restore. This can for instance be to have a
local backup on each users laptop and educate users on how they can
restore files from it.

### Ransomware

The Evolution of Ransomware in
figure [3.2](#fig:storage:77f6f2c86fdf4437940b73a192066ea0).

![The Evolution of
Ransomware.<span label="fig:storage:77f6f2c86fdf4437940b73a192066ea0"></span>](/home/erikhje/office/kurs/secsrv/03-storage/tex/../img/ransomware.png)  
<span>[F-Secure by Mikael
Albrecht](https://blog.f-secure.com/ransomware-timeline-2010-2017/)</span>

Punchline: *Be prepared. What will be the biggest threat in 2030?*

Ransomware is one of the biggest cybersecurity threats. Ransomware is
highly relevant for us since it is a threat to business continuity in at
least two ways: 1. it disrupts production by encrypting data and 2. it
steals confidential data (company secrets) and threatens to disclose it.
This means that reliable backups with fast restore procedures are
necessary to protect against threat number one, and protection of
backups is one of the measure to counter threat number two.

## Backup: What?

Data Analysis in
figure [\[fig:storage:b93e10dd69cb47b6a5c1420f30b2f7f5\]](#fig:storage:b93e10dd69cb47b6a5c1420f30b2f7f5).

from Æleen Frisch 

  - *Which files* need to be backed up? (which files or file types
    should be *ignored*)?

  - Where, when and under what conditions should backups be performed?

  - How often do these files change? What is *a change*?

NSM measure 2.9.2: Remember *configs, software, OS images, installation
software*

It is important to carefully estimate the value of the information you
have stored in your files. Some files are more important than others,
e.g. source code is much more important than a compiled executable file.
When you know which files to back up, you should analyze how much space
these files occupy and how frequently they change. This analysis builds
the ground for designing the backup system. We will not go into the
exact computations, value estimations and risk assessments needed to
build a complete backup system in this course. We focus on the basic
understanding of how backups work and what we need to do to have the
necessary basic protection.

### Exclude/Ignore files

Not every file should be backup up, so it is important to set up a list
of what we want to exclude/ignore from the backup. This list can be
files, directories or file/directory names that match certain patterns.
A simple algorithm for doing this is:

1.  Create a basic list of what is the most obvious to exclude.

2.  Do a backup. Look for log messages of errors or warnings. Examine
    the backup and look for unwanted files.

3.  Modify your list of what to exclude.

4.  Repeat until you are happy.

A typical list to start with of you are setting up a full backup of a
Windows-host, would look something like this:

    C:$$Recycle.Bin
    C:\pagefile.sys
    C:\hiberfil.sys
    C:\swapfile.sys
    C:\ProgramData\Microsoft\Windows Defender\Definition Updates\Backup\*
    C:\ProgramData\Microsoft\Windows Defender\Support\*.log
    C:\Windows\Temp\MpCmdRun.log
    C:\Windows\SoftwareDistribution\DataStore\Logs\*
    C:\Users\*\AppData\Local\Temp\Diagnostics\*
    C:\Windows\SoftwareDistribution\Download\*
    C:\ProgramData\Microsoft\Network\Downloader*
    C:\Windows\system32\LogFiles\WMI\RtBackup\*.*
    C:\Windows\memory.dmp
    C:\System Volume Information\*

### File Change Detection

File Change Detection in
figure [\[fig:storage:3678891e1446452fb9111be71d16c2af\]](#fig:storage:3678891e1446452fb9111be71d16c2af).

  - Actual content changed? (scanning for this takes time...)

  - Only scan content if a "B-MAC" change?
    
      - *B*irth (`CreationTime`)
    
      - *M*odified (`LastWriteTime`)
    
      - *A*ccess (`LastAccessTime`)
    
      - *C*hange (only change in metadata)

*Read it without updating AccessTime?*

Always scanning files for content changes is very expensive for large
file collections. Sometimes it is necessary to do it, and to be
absolutely certain of detecting any changes it must be done. Most of the
time it is sufficient to only scan for changes in Modified time. Restic
on other platforms than Windows checks if Modified time, Change time,
File Size and "File-ID" all match the previous version of the file if
Restic is to consider the file as not changed . For Windows environment
the Restic documentation states:

> On Windows, a file is considered unchanged when its path, size and
> modification time match …

## Backup: How?

### Tools

Restic.net in
figure [3.3](#fig:storage:cbec3b9dd2df46a3b0fb4f84996b77ba).

![Restic.net.<span label="fig:storage:cbec3b9dd2df46a3b0fb4f84996b77ba"></span>](/home/erikhje/office/kurs/secsrv/03-storage/tex/../img/restic.png)

There are many backup tools available. We have chosen to Restic in the
exercises since it is widely used, cross-platform and supports using
OpenStack Object Storage as a storage repository.

### Architecture

Architecture for Ransomware Protection in
figure [3.4](#fig:storage:51236aab97124d96aced364612acaa02).

![Architecture for Ransomware
Protection.<span label="fig:storage:51236aab97124d96aced364612acaa02"></span>](/home/erikhje/office/kurs/secsrv/03-storage/tex/../img/arch.pdf)

A key problem we need to solve when setting up a backup system
architecture is that backups should be *immutable*, meaning they should
not be possible to tamper with. If we are attacked by malware (e.g.
ransomware) and the malware is able to run with the permissions of the
backup account, the threat is that the malware will overwrite or encrypt
our backups. To protect against this threat we have at least three
different architectural options:

  - Air gap  
    means to have an "air gap" (and no wireless connection either of
    course) between our data and the backup storage, e.g. when you have
    your backup on a hard drive that is not connected in any way to your
    computer.

  - Intermediary server  
    means to have a dedicated backup server which a local backup process
    on your computer can write to, but a separate process on the backup
    server will copy/move, verify and secure your backup as soon as you
    have written to it. This way the backup server can move a copy out
    of reach for any malware on your computer.

  - Immutable storage  
    means for the backup process on your computer to have "append only"
    permission to the backup storage. In this way, malware can add
    encrypted data (or malware) to the backups but not influence
    previously backed up data.

In the exercises we will set up backup similar to "Immutable storage"
when we store backups in the object store of SkyHiGh.

### Full or Partial

Full or Partial in
figure [\[fig:storage:4714f3847e924a4caeaa47223132af24\]](#fig:storage:4714f3847e924a4caeaa47223132af24).

  - Full (level 0)  
    Copy all files

  - Partial  
    Two concepts sometimes referred to as the same:
    
      - Differential (level 1)  
        All new or modified files since last full backup
    
      - Incremental (level \(n\))  
        All new or modified files since last full *or partial* backup
    
    <!-- end list -->
    
      - Difference between differential and incremental sometimes
        referred to as *(dump) level \(n\)* where \(n\in{0,1,2,\ldots}\)

Dump levels refer to how many backups are needed to do a full restore.
If the most recent backup performed was a level three, then you would
need to do the following for a full restore:

1.  Restore last full backup (level 0)

2.  Restore last level 1 incremental backup

3.  Restore last level 2 incremental backup

4.  Restore last level 3 incremental backup

Doing a full backup takes much more time than doing a partial backup, so
"dump level strategy" is based on a trade-off between backup-time and
restore-time. These concepts (differential and incremental) are mostly
relevant when talking about tape backups, since reading from multiple
tapes can be quite time-consuming. When backing up to hard drive based
storage solutions (including cloud storage) we talk about *snapshots*
instead of increments. Snapshots are in principle increments, but they
are implemented in a way that is transparent to the user when doing
restore. A snapshot is a copy of your data at a certain point in time
(although the mechanism behind the scene is similar to an increment).

### 3-2-1 plan

3-2-1 Backup Plan in
figure [3.5](#fig:storage:5cc631b68ba7489482511e6a4d3ce3e9).

![3-2-1 Backup
Plan.<span label="fig:storage:5cc631b68ba7489482511e6a4d3ce3e9"></span>](/home/erikhje/office/kurs/secsrv/03-storage/tex/../img/3-2-1.pdf)

The 3-2-1 backup plan was coined by photographer Peter Krogh in the book
"The DAM Book: Digital Asset Management for Photographers"  and is a
general concept for having what should be a minimum of protection for
your data. It states that there should be three copies of your data
(typically one in production and two backups), two different storage
media should be in use (e.g. hard drive and magnetic tape) and one copy
should be off-site (in case of fire or similar incidents). We should
think of this rule as a minimum, e.g. the author practices a 4-3-2
version with production data in home directory, local backup on same
SSD, [remote backup on NTNU’s file
server](https://i.ntnu.no/wiki/-/wiki/English/File+Backup) and NTNU has
a backup system for the file server (probably a "tape robot" of some
kind).

### Schedule

[NTNU’s web page on
backups](https://i.ntnu.no/wiki/-/wiki/English/File+Backup) state "A
backup is taken of every home directory, every night, so that files can
be restored if corrupted or deleted". This is considered the off-site
secure backup. In addition, you probably want to protect against
accidental deletion and have a local backup that takes a
snapshot/increment every half hour (or even more often).

### Backup user

Measure 2.6.5 "Minimer rettigheter på drifts-kontoer" of NSM’s
"Grunnprinsipper for IKT-sikkerhet 2.0"  is worth reciting in full:

1)  Etabler ulike kontoer til de ulike drifts-operasjonene (selv om det
    kanskje er samme person som reelt sett utfører oppgavene), slik at
    kompromittering av en konto ikke gir fulle rettigheter til hele
    systemet. Dvs. forskjellige drifts-kontoer for backup,
    brukeradministrasjon, klientdrift, serverdrift, mm.

2)  Begrens bruken av kontoer med domeneadmin rettigheter til kun et
    minimum av virksomhetens drifts-operasjoner. Spesielt bør kontoer
    med domene-admin rettigheter aldri benyttes interaktivt på klienter
    og servere (reduserer konsekvensene av "pass the hash" angrep).

3)  Unngå bruk av upersonlige kontoer ("backup\_arne" er bedre enn bare
    "backup") slik at man har god sporbarhet og lettere kan deaktivere
    kontoer når noen slutter. Hvis det er vanskelig å unngå å ha en
    upersonlig konto bør man først logge seg inn med en personlig bruker
    for å ivareta sporbarhet.

This means we should use a dedicated account to run the backup service
and this account should have a minimum of privileges.

Backup Account/User in
figure [\[fig:storage:3cc5d72b3f984fafae25b155e5bbe923\]](#fig:storage:3cc5d72b3f984fafae25b155e5bbe923).

  - Dedicated account
    
      - *Minimize risk* if compromised
    
      - *Traceability*

  - Minimum privileges
    
      - Must be able to *read all files*
    
      - Must be able to *read "files in use"*

On Linux this is most easily done by creating an account with the
password field set to an "invalid character" like `!` or `*`) and the
account’s login shell set to `/usr/sbin/nologin`. In addition, the
backup binary executable that will run in the security context of this
account can get a special privilege that allows it to read all files in
the file system. This can be done for e.g. the Restic-binary with
something like  
`setcap cap_dac_read_search=+ep /usr/bin/restic`  
(a problem still exists with this approach in that this `cap_dac_read`
capability must be set any time the Restic-binary is updated).

On Windows this is unfortunately a bit harder. The parallel to Linux
would be to create a user that is disabled and without a password:  
`New-LocalUser -Name BackupMysil -Disabled -NoPassword`  
seBackupPrivilege in
figure [3.6](#fig:storage:1fbe0fda72414fddb6d417318e83ed97).

![seBackupPrivilege.<span label="fig:storage:1fbe0fda72414fddb6d417318e83ed97"></span>](/home/erikhje/office/kurs/secsrv/03-storage/tex/../img/seBackupPrivilege.png)

The user `BackupMysil` should then be given a special *privilege* called
*seBackupPrivilege* to be able to read all files on the file system. On
Windows there exists by default a group called "Backup Operators":

{% highlight powershell %}
    PS> Get-LocalGroup -Name 'Backup Operators' | Select-Object -Property *
        
    Description     : Backup Operators can override security restrictions for 
                      the sole purpose of backing up or restoring files
    Name            : Backup Operators
    SID             : S-1-5-32-551
    PrincipalSource : Local
    ObjectClass     : Group
{% endhighlight %}

This group also has the privilege *seRestorePrivilege* which basically
is the right to write anywhere in the file system and we don’t want
that. If an attacker is able to compromise a process running under an
account with the *seRestorePrivilege*, they will be able to own the
system (take control of everything, become Administrator). We can verify
which groups and users have which privileges with the Sysinternals tool
`AccessChk` (notice how BackupMysil only has seBackupPrivilege while the
Backup Operators group also have the seRestorePrivilege):

{% highlight powershell %}
    PS> accesschk.exe -a seBackupPrivilege
      
              BUILTIN\Backup Operators
              BUILTIN\Administrators
              DC1\BackupMysil
    
    PS> accesschk.exe -a seRestorePrivilege
        
              BUILTIN\Backup Operators
              BUILTIN\Administrators
      
    # can also verify with
    secedit /export /areas USER_RIGHTS /cfg OUTFILE.CFG
    # and a logged in user can check its own privileges with
    whoami /PRIV
{% endhighlight %}

On Windows there also exists a special service that can be used for
reading "files in use". This service is called [Volume Shadow Service
(VSS)](https://docs.microsoft.com/en-us/windows-server/storage/file-server/volume-shadow-copy-service)
and [from the
documentation](https://docs.microsoft.com/en-us/windows/win32/vss/security-considerations-for-writers)
it should be possible to let an account use it by adding the account
name to the registry with a value of `1`:

{% highlight powershell %}
    PS> Get-Item HKLM:\SYSTEM\CurrentControlSet\Services\VSS\VssAccessControl\
    
    Name                           Property
    ----                           --------
    VssAccessControl               NT Authority\NetworkService : 1
                                   DC1\mysil                   : 1
{% endhighlight %}

Turning theory into practice is sometimes too hard. Unfortunately your
teacher have not been able to get this setup to work\[3\]. As you will
see in the exercises, we run the backup process under the `NT
Authority\SYSTEM` account which has a [full set of permissions to all
files](https://docs.microsoft.com/en-us/windows/security/identity-protection/access-control/local-accounts#system)
in the file system.

### Important features

#### Encryption

Encryption in
figure [\[fig:storage:1865cdadd2aa4987b7e67da50aaf7908\]](#fig:storage:1865cdadd2aa4987b7e67da50aaf7908).

{% highlight powershell %}
    PS> Get-Content confidential.txt 
    Some company secrets...

    PS> openssl enc -aes-256-cbc -a -pbkdf2 -in `
      confidential.txt -out confidential.txt.enc 
    enter aes-256-cbc encryption password:
    Verifying - enter aes-256-cbc encryption password:

    PS> Get-Content confidential.txt.enc 
    U2FsdGVkX1/1fC3YTahayHjr4Yy3G1byPrli5icqTwo4LHF...
{% endhighlight %}

Some of the data we need to back up are confidential, and we need to
protect it during transit and in rest (in storage). The best way to do
this is to encrypt it as early as possible, meaning encrypt it on the
client side before transmitting it over the network (although the
network connection is probably encrypted as well).

#### Compression

Compression in
figure [\[fig:storage:cb8cd02c5ab243fcabcb3b29a1738afc\]](#fig:storage:cb8cd02c5ab243fcabcb3b29a1738afc).

  - Lossless compression (not lossy like jpeg-images)

  - *Compress these 32 bits (4 Bytes) into 1 Byte?*  
    `10111011 10111011 10111011 10111011`

  - Same information content with Run-length encoding: `10001011`

This is just a simple example of compression, so we know how it is
possible to compress data. All files (unless already compressed) contain
repeating patterns of some kind that can be encoded more efficiently
that what is done in regular file storage.

#### Deduplication

Deduplication in
figure [3.7](#fig:storage:8a7aa34cee954d9ca2ebafc2dba0f4de).

![Deduplication.<span label="fig:storage:8a7aa34cee954d9ca2ebafc2dba0f4de"></span>](/home/erikhje/office/kurs/secsrv/03-storage/tex/../img/dedup.pdf)

Deduplication is similar to compression but on a much higher level.
Deduplication identifies high level data structures, typical entire data
blocks (or in very primitive implementations: entire files), and only
store one copy of each unique data structure.

## Secure Storage and Restore

### Verifying

Verifying in
figure [\[fig:storage:31f38c67df564d29b9d85a6739a415a9\]](#fig:storage:31f38c67df564d29b9d85a6739a415a9).

  - Structural consistency and integrity
    
      - `restic -r restic-repo check`

  - Integrity of the actual data
    
      - `restic -r restic-repo check --read-data`

A backup copy is not much worth if we cannot trust it. If the actual
storage device where the backup is located has been damaged (e.g. a
faulty RAID-volume) we can detect this by checking structural
consistency which means to check all the files used by the backup system
itself (index files and other data structures). If we are really unlucky
it can happen that an attacker have gained access to the backup storage
and written malware into the actual files (hoping that we will do a
restore and thereby spread the malware). To protect against this we have
to actually check the contents of the files (if it has been changed and
does not match e.g. hash sums stored in the metadata, and of course this
metadata can also be compromised in the attack…). This can also include
actual "virus scanning" of the backed up files. E.g. when you are
involved in recovering from a ransomware attack, how do you really know
that the ransomware is not present in the backup (which of the snapshots
do you trust? How far back in time do you have to go?).

### Restore

Restore in
figure [\[fig:storage:0114ac75f9c34cf595adf13c0c5de62b\]](#fig:storage:0114ac75f9c34cf595adf13c0c5de62b).

  - Restore everything?

  - Restore subdirectories only?

  - Browse and select files to restore?

Doing a full restore can be very expensive (time-consuming). Make sure
you use a backup system that provides flexible restore options.

## Review questions and problems

1.  (**KEY PROBLEM**) Consider the setup we have done with Restic in the
    lab tutorial. This does not follow the ideal setup with respect to
    user accounts and permissions/privileges. In what way is our setup
    bad? In what we is our setup good?

2.  Write a command line that will show all volumes that have more than
    5GB size remaining.

3.  Write a command line that will output the following (note: sorted by
    size)
    
        DiskNumber PartitionNumber        Size
        ---------- ---------------        ----
                 1               3       66048
                 1               1    16693760
                 2               1    16759808
                 2               2  4294967296
                 2               3  6424625152
                 1               2 10719592448
                 0               1 32210157568

4.  (**KEY PROBLEM**) Before initiating a backup scheme, we need to know
    a bit about the data we would like to back up. Create PowerShell
    command lines to answer the following questions:
    
    1)  How much space does your home directory use including all files
        and directories?
    
    2)  Which file is the largest? Which is the smallest?
    
    3)  How many files are there? What is the average file size?
    
    4)  How many files changed and how much space did those files use in
        total, during the last hour? last day? last week? (hint: see
        [examples in the Sort-Object section of the PowerShell
        tutorial](https://gitlab.com/erikhje/dcsg1005/-/blob/master/powershell.md#sort-object))
    
    5)  (Advanced) Which of the last seven days had the largest change?
        (largest meaning total space used by all files that was changed
        during those 24 hours)
    
    (tip: remember that PowerShell is cross-platform, so maybe try this
    out on your own laptop to have a more realistic setting than what we
    have in our virtual machines.)

5.  **Automate backup**. Do the lab tutorial. The goal of this exercise
    is to wrap Restic in a PowerShell script and let it run every half
    hour by using Task Scheduler.
    
    1)  Create a new folder `scripts` in your home directory and in this
        folder create
        
          - a file `excludes.txt` with all the file patterns you want to
            exclude from your backup (see example earlier in this
            chapter)
        
          - a file `mybackup.ps1` where you set all environment
            variables and run the backup command from the lab tutorial  
            (hint: use `$env:RESTIC_PASSWORD` to set the repository
            password)
    
    2)  At the end of your script, add commands for deleting the
        environment variables containing secrets (think "just in time",
        these secrets are only needed when restic is executed).
    
    3)  Schedule the script to run every half hour with the following
        commands (must be in a PowerShell as Administrator):
        
            $taskName = "MyBackup"
            $taskDescription = "Restic backup of Users data"
                
            $taskAction = New-ScheduledTaskAction `
              -Execute 'C:\Program Files\PowerShell\7\pwsh.exe' `
              -Argument '-File C:\Users\Admin\scripts\mybackup.ps1'
                
            $taskTrigger = New-ScheduledTaskTrigger `
              -Once -At (Get-Date) `
              -RepetitionInterval (New-TimeSpan -Minutes 30) `
              -RepetitionDuration (New-TimeSpan -Days (7))
            
            # Note: Really should use another account, but
            # too hard to get it to work...
            $taskPrincipal = New-ScheduledTaskPrincipal `
              -UserId "NT AUTHORITY\SYSTEM" `
              -LogonType ServiceAccount
                
            Register-ScheduledTask `
              -TaskName $taskName `
              -Description $taskDescription `
              -Action $taskAction `
              -Trigger $taskTrigger `
              -Principal $taskPrincipal
    
    4)  Run your registered task manually once using
        [Start-ScheduledTask](https://docs.microsoft.com/en-us/powershell/module/scheduledtasks/start-scheduledtask).
    
    5)  Study the [Restic
        manual](https://restic.readthedocs.io/en/latest/040_backup.html#comparing-snapshots)
        and run a command to show the difference between two of your
        snapshots.
    
    6)  Use
        [Get-ScheduledTaskInfo](https://docs.microsoft.com/en-us/powershell/module/scheduledtasks/get-scheduledtaskinfo)
        to print only TaskName, LastTaskResult and NextRunTime for your
        registered task.
    
    7)  Use
        [Get-ScheduledTask](https://docs.microsoft.com/en-us/powershell/module/scheduledtasks/get-scheduledtask)
        to print only the path to pwsh.exe for your registered task
        (hint: this is the property Execute "under" the
        Actions-property, use Get-Member to see that the
        Actions-property is not just a string).
    
    8)  Open PowerShell as Administrator, create the directory  
        `C:\restoretest` and [do a restore with
        restic](https://restic.readthedocs.io/en/latest/050_restore.html)
        of your latest snapshot to this directory.

## Lab tutorials

1.  **Attach a block device on D:**. This exercise is not mandatory, but
    if you are unsure what a block device is and how disks and file
    systems show up in Windows, then do it.
    
    1.  Login to SkyHiGh, click "Volumes", "Volumes", "Create Volume",
        give it any name you want and leave it to 1GB in size.
    
    2.  To the right of your newly created volume click "Edit Volume",
        "Manage Attachments", in "Attach to Instance" choose the server
        you would like to attach the volume to.
    
    3.  On the server do the following to make this volume accessible on
        `D:`:
        
            # Find disk number (in PowerShell):
            Get-Disk
            # Initialize
            Initialize-Disk DISK_NUMBER
                
            # Partition and format
            New-Partition -DiskNumber DISK_NUMBER -UseMaximumSize `
              -AssignDriveLetter | Format-Volume
                
            # View
            Get-PSDrive
    
    4.  If you want to "safely remove" the attached volume, do
        
            # Unmount (if Drive has letter 'D')
            Get-Volume -Drive D | Get-Partition |
              Remove-PartitionAccessPath -AccessPath D:\

2.  **Backup to (immutable) object storage**. Let’s try to set up the
    backup software Restic on a Windows Server and use OpenStack’s
    object storage (Swift) as backend (storage repository for the
    backups).
    
    1.  If you don’t already have a Windows Server to use, create a new
        stack and log in to the server (see lab tutorial in week one).
    
    2.  Create the object store container where the backups will be
        stored. Login to SkyHiGh, click "Object Store", "Containers",
        "+Container", name it `mysil` (you can name it whatever you
        want, but if you name it mysil you can copy and paste more of
        the text later).
    
    3.  Many times when we need to automate tasks, we need to have
        authentication, which typically means putting a password into a
        script. Password in scripts or configuration files is not
        something we want, but we cannot always avoid this, so the best
        we can do is use a separate account with minimum privileges. For
        the current task, let’s create a separate account which is valid
        only for a short time period (e.g. the month of January). In
        SkyHiGh, click "Identity", "Application Credentials", "Create
        Application Credential":
        
          - Choose a name, e.g. "backupcreds"
        
          - Set expiration date to last day of January.
        
          - Click on role "\_member\_".
        
          - Click "Create Application Credentials"
        
          - Click "Download openrc file". In this file you will find the
            ID and the Secret (the "password").
    
    4.  Inspect [Restic
        package](https://community.chocolatey.org/packages/restic) (does
        it look safe to install?), Open PowerShell as Administrator,
        install Restic with  
        `choco install -y restic`.
    
    5.  Open PowerShell (not as Administrator), set the necessary
        environment variables (replace `YOUR_ID` and `YOUR_SECRET`):
        
            $env:OS_AUTH_TYPE='v3applicationcredential'
            $env:OS_AUTH_URL='https://api.skyhigh.iik.ntnu.no:5000/v3'
            $env:OS_IDENTITY_API_VERSION='3'
            $env:OS_REGION_NAME="SkyHiGh"
            $env:OS_INTERFACE='public'
            $env:OS_APPLICATION_CREDENTIAL_ID='YOUR_ID'
            $env:OS_APPLICATION_CREDENTIAL_SECRET='YOUR_SECRET'
    
    6.  Initialize your backup repository (you will now need to provide
        a password of your choice, which is used for encrypting your
        backups to protect them) with  
        `restic -r swift:mysil:/ init`
    
    7.  Confirm that your backup repository is empty (it does not have
        any snapshots) with  
        `restic -r swift:mysil:/ snapshots`
    
    8.  Open PowerShell as Administrator (so we can use the Volume
        Shadow Service (VSS) to also backup files that are in use),
        create a backup of your home directory with  
        `restic -r swift:mysil:/ backup C:\Users\Admin
        --use-fs-snapshot`  
        (remember that you have to set our environment variables
        whenever you open a new PowerShell session)
    
    9.  Confirm that your backup repository now have one snapshot with  
        `restic -r swift:mysil:/ snapshots`

# Git, Markdown and CI/CD

## TL;DR

TL;DR in
figure [\[fig:git:726069d2f4364f76b7ef73dc8d768815\]](#fig:git:726069d2f4364f76b7ef73dc8d768815).

  - Watch the 20-minute [Git-Lecture from
    IfI,UiO](https://youtu.be/vWdmXunGQC8) (from the course
    [IN3110](https://uio-in3110.github.io))

  - Study the [Git Cheat
    Sheet](https://about.gitlab.com/images/press/git-cheat-sheet.pdf)

  - Study the [Markdown Cheat
    Sheet](https://www.markdownguide.org/cheat-sheet) and browse the
    [GitLab Flavored
    Markdown](https://docs.gitlab.com/ee/user/markdown.html)

  - Figure out what
    [.gitlab-ci.yml](https://gitlab.com/erikhje/heat-mono/-/blob/master/.gitlab-ci.yml)
    is used for

Note: from the cheat sheet (and the video), you need to learn properly:

  - 01 Git configuration

  - 02 Starting A Project

  - 03 Day-To-Day Work (not `git stash`)

  - 05 Review your work (only `git log`)

  - 08 Synchronizing repositories (not `git fetch`)

  - D The zoo of working areas (don’t worry about stash or branches)

  - theory of file stages

  - and later how to deal with conflicts

## Version Control with Git

It’s not a joke in
figure [4.1](#fig:git:5a2514efe967490fb53182e427d81461).

![It’s not a
joke.<span label="fig:git:5a2514efe967490fb53182e427d81461"></span>](/home/erikhje/office/kurs/secsrv/04-git/tex/../img/git.png)

Everyone in IT uses git, but very few really know the details. You do
memorize a few commands and look up the details when you need them. Note
that Git was created in 2005 by Linus Torvalds together with other Linux
kernel developers with the purpose of having a better version control
system for the source code of the Linux operating system kernel .

### Repository

An Empty but Initialized Repo in
figure [4.2](#fig:git:47dce43f0d3a426986e01f996590d172).

![An Empty but Initialized
Repo.<span label="fig:git:47dce43f0d3a426986e01f996590d172"></span>](/home/erikhje/office/kurs/secsrv/04-git/tex/../img/gittree.png)

Git is two things: 1. the Git repository (repo) and 2. the Git program.
The Git repo is just a directory with one special hidden directory:
`.git`. This is the directory where all the git data structures are
stored, and we typically never edit these files manually, they are
changed by using the Git program.

### File States

Four States of a File in
figure [4.3](#fig:git:987115a3366945ca82a7d2eb9e4d51a9).

![Four States of a
File.<span label="fig:git:987115a3366945ca82a7d2eb9e4d51a9"></span>](/home/erikhje/office/kurs/secsrv/04-git/tex/../img/gitstates.pdf)

You can have a file in the git repo that git does not know about. This
is called an *Untracked* file. For the files that have been added to git
(the *tracked* files) they can be *Unmodified* like `README.md` and
`test.ps1` in the figure, or *Modified but not staged* like `b.txt`, or
they can be newly added or modified and *Staged*.

Modified but not staged means they will not be part of the next commit
if you do `git commit -m "msg"`, but they will be added to the staged
state and committed if you do `git commit -am "msg"`. The manual of
git-commit states about the option `-a` or `–all`:

> Tell the command to automatically stage files that have been modified
> and deleted, but new files you have not told Git about are not
> affected.

All files that are *Staged* will be part of the next commit (the next
version of the repo).

### Workflow

Typical Workflow in
figure [\[fig:git:2756a6c467fc43ca8030b54bc51fe6ab\]](#fig:git:2756a6c467fc43ca8030b54bc51fe6ab).

1.  `git pull origin master`

2.  edit a file

3.  `git commit -am "docs: spellcheck Foo"`  
    *[conventionalcommits.org](https://www.conventionalcommits.org/)\!*

4.  `git push origin master`

When you work on a project you pull the latest version, make small
changes, commit and push the changes into the shared repo. Note: it is
important to understand that there is nothing special about the
repository in GitLab or GitHub. They are just the same git repo as you
have locally. Git is fully decentralized. It is just convenient to have
one repository that we share and can push to.

Always think that others will at some point in time take over your code.
Try to write readable high quality code and documentation, and also try
to put some effort into making sensible commit messages: browse the web
page  
[https://www.conventionalcommits.org](https://www.conventionalcommits.org/)  
and try to follow that standard when you write commit messages.

### Conflicts

Conflicts in
figure [\[fig:git:3885888439fd41ca98a99aae0b06a123\]](#fig:git:3885888439fd41ca98a99aae0b06a123).

  - *Avoid them by working on separate files if possible.*

  - Browse [Merge
    conflicts](https://docs.gitlab.com/ee/user/project/merge_requests/conflicts.html)
    if git does not resolve them automatically

It can happen that two or more people make changes to the same files,
and then try to push these changes. Git then either resolves this if the
changes are in different parts of the file, but if they are overlapping
changes you have to deal with them manually.

## Markdown

The Magic of "View Source" in
figure [4.4](#fig:git:3e93d5a85b2544d6b6a02212f8790a80).

![The Magic of "View
Source".<span label="fig:git:3e93d5a85b2544d6b6a02212f8790a80"></span>](/home/erikhje/office/kurs/secsrv/04-git/tex/../img/mdsource.png)

Markdown is something you learn by spending 10 minutes reading the
[Markdown Cheat Sheet](https://www.markdownguide.org/cheat-sheet/) (the
"Basic Syntax" takes you a long way), looking up [GitLab Flavored
Markdown](https://docs.gitlab.com/ee/user/markdown.html) when you want
to do something fancy, and otherwise just studying the source code of
others. Markdown is much simpler than other markup languages, and is to
go-to language for any quick notes or documentation you need to write
(especially in context of a git repo).

## CI/CD

Waterfall (OLD\!) in
figure [4.5](#fig:git:f3b1d6ba51bd4d4ca3d309ea35329bb7).

![Waterfall
(OLD\!).<span label="fig:git:f3b1d6ba51bd4d4ca3d309ea35329bb7"></span>](/home/erikhje/office/kurs/secsrv/04-git/tex/../img/waterfall.pdf)  
<span>Peter Kemp / Paul Smith, [CC
BY 3.0](https://creativecommons.org/licenses/by/3.0), via Wikimedia
Commons</span>

When the author was taught software engineering in 1993, Waterfall was
the dominant model. Waterfall is a sequential model with the steps
Requirement, Design, Implementation, Verification, Maintenance (or some
variation thereof). In other words, it leads to careful and slow
development processes which would lead to a final product being
implemented in production maybe once or twice per year. It did not
facilitate close cooperation between *Developers* and *Operations* (the
system and network administrators).

DevOps (TODAY\!) in
figure [4.6](#fig:git:2bc87168dba84154873cc46d5894cbd4).

![DevOps
(TODAY\!).<span label="fig:git:2bc87168dba84154873cc46d5894cbd4"></span>](/home/erikhje/office/kurs/secsrv/04-git/tex/../img/devops.pdf)  
<span>Kharnagy, [CC
BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0), via
Wikimedia Commons</span>

The DevOps model which we use today facilitates close cooperation
between *Developers* and *Operations* (the system and network
administrators). The model is possible much due to virtualization and
cloud computing software, since they allow for fast and frequent changes
in production. It is common today that we make changes in production
software every week or even more than once per day in some cases. Since
security has been an undervalued topic for too many years, some people
refer to *DevSecOps* as a separate model, but it is really just about
incorporating security into every step of the DevOps-model which is
something we should be doing anyway.

### Pipeline

CI/CD Pipeline in
figure [4.7](#fig:git:a380ad0b290f49729640018bf5d5a169).

![CI/CD
Pipeline.<span label="fig:git:a380ad0b290f49729640018bf5d5a169"></span>](/home/erikhje/office/kurs/secsrv/04-git/tex/../img/pipeline.jpg)  
<span>Jouasse, [CC
BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0), via
Wikimedia Commons</span>

See our example
[.gitlab-ci.yml](https://gitlab.com/erikhje/heat-mono/-/blob/master/.gitlab-ci.yml)

To make DevOps happen, with frequent changes in production software, we
have to automate everything that we can automate. This applies
specifically to testing in all shapes and sizes. We should automate
testing of code quality (code comments, code style, best practice
checks), bugs and bad code, security and maybe even do performance
testing. Testing is done in a sequence of steps, this is called a *CI/CD
pipeline*. CI is short for *Continuous Integration* while CD is short
for either *continuous delivery* (mostly used by developers) or
*continuous deployment* (mostly used by operations). This is a quite
complicated topic, and you will learn more about it in later courses.
The basics we stick with now is that we set up a test (a CI/CD-pipeline
with just one step) that runs every time we push our git changes to the
central git repository. The only thing this pipeline does is check if
our PowerShell scripts passes the
[PSScriptAnalyzer](https://github.com/PowerShell/PSScriptAnalyzer) (a
*static code checker* for PowerShell) without it triggering any
warnings.

## Review questions and problems

1.  The file `file.md` is in state *Unmodified* in a git-repo. What
    happens when you run the command `git rm file.md`?

2.  (**KEY PROBLEM**) Create a new directory, change into it and create
    an empty git repo with  
    `git init`  
    Create the following status in the git repo (in other words: create
    the four files and run git commands so the status ends up being just
    like this):
    
        PS> Get-ChildItem *.txt | Format-Table -Property Name
        
        Name
        ----
        a.txt
        b.txt
        c.txt
        d.txt
        
        PS> git status
        
        On branch main
        Your branch is ahead of 'origin/main' by 1 commit.
        (use "git push" to publish your local commits)
        
        Changes to be committed:
        (use "git restore --staged <file>..." to unstage)
              modified:   a.txt
              new file:   c.txt
        
        Changes not staged for commit:
        (use "git add <file>..." to update what will be committed)
        (use "git restore <file>..." to discard changes in working directory)
              modified:   b.txt
        
        Untracked files:
        (use "git add <file>..." to include in what will be committed)
              d.txt

## Lab tutorials

1.  Install git with `choco install -y git` (as always, [check if
    package is OK
    first](https://community.chocolatey.org/packages/git)). Do "01 Git
    configuration" from the [Git Cheat
    Sheet](https://about.gitlab.com/images/press/git-cheat-sheet.pdf).

2.  **Shared and Local Repo**.
    
    1.  Log in on <https://gitlab.stud.idi.ntnu.no> and create a new git
        repository with "New Project", "Create blank project", name it
        mysil, (leave it private, and leave it to init with README),
        "Create project". You now have a remote git repo which you want
        to have a copy of locally AND be able to make changes and upload
        them *without using a password every time*. The following adds a
        ssh public-key to your account. If you want to have a different
        setup, e.g. a separate key for just your repo, then [read the
        docs](https://docs.gitlab.com/ee/ssh/).
    
    2.  (Do this on your laptop or on a Windows host in SkyHiGh)  
        `ssh-keygen -t rsa -b 2048 -C "gitlab.idi projects"`  
        Name the file as proposed (if you use another name, you have to
        setup a config file) and leave a blank password (this means that
        if anyone gets hold of your private key, they will have write
        access to your repo).  
        `Get-Content C:\Users\Admin\.ssh\id_rsa.pub | Set-Clipboard`
    
    3.  (Do this in GitLab) Click your avatar (top right corner),
        "Preferences", "SSH keys" and paste your SSH public key which
        you just put on your clipboard.
    
    4.  (Do this on your laptop or on a Windows host in SkyHiGh)  
        
            git clone git@gitlab.stud.idi.ntnu.no:erikhje/mysil.git
            cd mysil
            git status
            Write-Output "A small edit" >> README.md
            Write-Output "# Script TBA" > test.ps1
            git status
            git add test.ps1
            git status
            git commit -am "Added a file and a small edit"
            git status
            git push origin main
    
    5.  Visit your project in GitLab to see the changes and view
        "History" to see the commits (versions).

3.  **Conflicts**. Add some of your fellow students to your newly
    created git project, have everyone clone the repo, make changes to
    the same file and push those changes. Study [Merge
    conflicts](https://docs.gitlab.com/ee/user/project/merge_requests/conflicts.html)
    and try to solve the conflicts that probably appears.

<!-- end list -->

1.  SkyHiGh was born in 2011 as a cloud ("Sky") at Høgskolen i Gjøvik
    (HiG).

2.  Note: Process Explorer is widely used, see [the discovery of this
    bug](https://bugs.chromium.org/p/chromium/issues/detail?id=1254631)
    (rewarded $3000).

3.  Maybe you can get it to work? See the exercises, maybe try the
    `Network Service` account with added seBackupPrivilege? You will
    receive an award if you are able to solve it.
